﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TicTacToeClient
{
    public class RoomPlayer : MonoBehaviour
    {
        [SerializeField] private Text playerNicknameText;
        [SerializeField] private Image playerReadyImage;

        public void SetPlayerNickname(string nickname)
        {
            playerNicknameText.text = nickname;
        }

        public void SetPlayerReady(bool isReady)
        {
            if (isReady)
            {
                playerReadyImage.color = Color.green;
            }
            else
            {
                playerReadyImage.color = Color.red;
            }
        }

    }
}
