﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TicTacToeClient
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager instance;

        public GameObject MarkXPrefab;
        public GameObject MarkOPrefab;

        public bool isStarted;

        private GameObject cellsHolder;

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else if (instance != this)
            {
                Debug.LogWarning("Instance already exists, destroying object!");
                Destroy(this);
            }
        }

        public void MakeMove(bool isXTurn, int x, int y)
        {
            GameObject prefab = isXTurn ? MarkXPrefab : MarkOPrefab;
            Vector3 position = new Vector3(x - 1, 0, y - 1) * 2.5f;
            GameObject mark = Instantiate(prefab, position, Quaternion.identity, cellsHolder.transform);
        }

        public void StartGame()
        {
            ResetMap();

            isStarted = true;
        }

        private void ResetMap()
        {
            if (cellsHolder != null)
                Destroy(cellsHolder);

            cellsHolder = new GameObject("Cells Holder");
            cellsHolder.transform.SetParent(transform);
        }

        public void EndGame()
        {
            
        }

    }
}