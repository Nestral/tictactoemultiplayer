﻿using UnityEngine;

namespace TicTacToeClient
{
    public class ClientSend
    {
        private static void SendTCPData(Packet packet)
        {
            packet.WriteLength();
            Client.instance.tcp.SendData(packet);
        }

        #region Packets
        public static void WelcomeReceived()
        {
            using (Packet packet = new Packet((int)ClientPackets.welcomeReceived))
            {
                packet.Write(Client.instance.myId);
                packet.Write(UIManager.instance.usernameField.text);

                SendTCPData(packet);
            }
        }

        public static void RoomsStateRequest()
        {
            using (Packet packet = new Packet((int)ClientPackets.roomsStateRequest))
            {

                SendTCPData(packet);
            }
        }

        public static void MakeMoveRequest(Vector2 position)
        {
            using (Packet packet = new Packet((int)ClientPackets.makeMoveRequest))
            {
                packet.Write((int)position.x);
                packet.Write((int)position.y);

                SendTCPData(packet);
            }
        }

        public static void MakeNewRoomRequest()
        {
            using (Packet packet = new Packet((int)ClientPackets.createNewRoomRequest))
            {
                SendTCPData(packet);
            }
        }

        public static void ConnectToRoomRequest(int roomId)
        {
            using (Packet packet = new Packet((int)ClientPackets.playerJoinRoomRequest))
            {
                packet.Write(Client.instance.myId);
                packet.Write(roomId);

                SendTCPData(packet);
            }
        }

        public static void LeaveRoomRequest()
        {
            using (Packet packet = new Packet((int)ClientPackets.playerLeaveRoomRequest))
            {
                packet.Write(Client.instance.myId);

                SendTCPData(packet);
            }
        }

        public static void ChangeReadyStateRequest()
        {
            using (Packet packet = new Packet((int)ClientPackets.playerReadyStateChangeRequest))
            {
                packet.Write(Client.instance.myId);

                SendTCPData(packet);
            }
        }
        #endregion
    }
}