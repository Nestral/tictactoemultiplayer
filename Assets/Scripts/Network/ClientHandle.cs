﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;

namespace TicTacToeClient
{
    public class ClientHandle
    {
        public static void Welcome(Packet packet)
        {
            Client.instance.isConnected = true;

            string msg = packet.ReadString();
            int myId = packet.ReadInt();

            Debug.Log($"Connected to server successfully. {msg}");
            Client.instance.myId = myId;
            ClientSend.WelcomeReceived();

            UIManager.instance.OnConnectedToServer();
        }

        public static void RoomsStateReceived(Packet packet)
        {
            //Clear old information
            UIManager.instance.ClearInformation();

            //Read rooms' current state
            int roomCount = packet.ReadInt();
            for (int i = 0; i < roomCount; i++)
            {
                int roomId = packet.ReadInt();

                RoomManager room = new RoomManager(roomId);
                UIManager.instance.rooms.Add(roomId, room);

                int playersCount = packet.ReadInt();

                for (int j = 0; j < playersCount; j++)
                {
                    int player = packet.ReadInt();
                    room.PlayerJoin(player);
                    UIManager.instance.playerRooms.Add(player, room);

                    bool ready = packet.ReadBool();
                    room.PlayerChangeReadyState(player, ready);

                }
            }

            //Update UI
            UIManager.instance.OnRoomsStateInfoReceived();
        }

        public static void MoveReceived(Packet packet)
        {
            bool isXTurn = packet.ReadBool();
            int x = packet.ReadInt();
            int y = packet.ReadInt();

            GameManager.instance.MakeMove(isXTurn, x, y);
        }

        public static void RoomCreated(Packet packet)
        {
            int clientId = packet.ReadInt();
            int roomId = packet.ReadInt();

            UIManager.instance.OnRoomCreated(clientId, roomId);
        }

        public static void PlayerConnectedToRoom(Packet packet)
        {
            int clientId = packet.ReadInt();
            int roomId = packet.ReadInt();

            UIManager.instance.OnPlayerConnectedToRoom(clientId, roomId);

            Debug.Log($"Player {clientId} joined the room.");
        }

        public static void PlayerLeftRoom(Packet packet)
        {
            int playerId = packet.ReadInt();

            UIManager.instance.OnPlayerDisconnectedFromRoom(playerId);

            Debug.Log($"Player {playerId} left the room.");
        }

        public static void PlayerReadyStateChangeReceived(Packet packet)
        {
            int playerId = packet.ReadInt();
            bool ready = packet.ReadBool();

            UIManager.instance.OnPlayerReadyStateChanged(playerId, ready);

            Debug.Log($"Player {playerId} has changed his ready state.");
        }

        public static void GameStartReceived(Packet packet)
        {
            UIManager.instance.OnGameStarted();

            Debug.Log($"The game has started.");
        }

        public static void GameEndReceived(Packet packet)
        {
            UIManager.instance.OnGameEnded();

            Debug.Log($"The game has ended.");
        }
    }
}