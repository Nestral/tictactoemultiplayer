﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Net;

namespace TicTacToeServer
{
    class Server
    {
        public static int MaxPlayers { get; private set; }
        public static int Port { get; private set; }

        public static Dictionary<int, Client> clients = new Dictionary<int, Client>();
        public static Dictionary<int, RoomManager> rooms = new Dictionary<int, RoomManager>();

        public delegate void PacketHandler(int fromClient, Packet packet);
        public static Dictionary<int, PacketHandler> packetHandlers;

        private static TcpListener tcpListener;
        private static UdpClient udpListener;

        public static void Start(int maxPlayers, int port)
        {
            MaxPlayers = maxPlayers;
            Port = port;

            Console.WriteLine("Server starting...");
            InitializeServerData();

            tcpListener = new TcpListener(IPAddress.Any, Port);
            tcpListener.Start();
            tcpListener.BeginAcceptTcpClient(new AsyncCallback(TCPConnectCallback), null);

            udpListener = new UdpClient(Port);
            udpListener.BeginReceive(UDPReceiveCallback, null);

            string serverIP = new WebClient().DownloadString("http://icanhazip.com").TrimEnd(); //Dns.GetHostEntry(Dns.GetHostName()).AddressList[0].ToString();
            Console.WriteLine($"Server started. IP: {serverIP}. Port: {Port}.");
        }

        public static RoomManager CreateNewRoom(int fromClient)
        {
            //Create new room
            int newRoomId = rooms.Count + 1;
            RoomManager newRoom = new RoomManager(2, 2, newRoomId);
            rooms.Add(newRoomId, newRoom);

            //Connect player to room
            newRoom.PlayerJoin(fromClient);
            clients[fromClient].room = newRoom;

            return newRoom;
        }

        public static void RemoveRoom(RoomManager room)
        {
            Console.WriteLine($"Removing room {room.roomId}");

            //Disconnect players if some are left in the room
            foreach (int client in room.playersReady.Keys)
            {
                clients[client].room = null;
            }

            //Remove room
            rooms.Remove(room.roomId);
        }


        private static void TCPConnectCallback(IAsyncResult result)
        {
            TcpClient client = tcpListener.EndAcceptTcpClient(result);
            tcpListener.BeginAcceptTcpClient(new AsyncCallback(TCPConnectCallback), null);

            Console.WriteLine($"Incoming connection from {client.Client.RemoteEndPoint}...");

            for (int i = 1; i <= MaxPlayers; i++)
            {
                if (clients[i].tcp.socket == null)
                {
                    clients[i].tcp.Connect(client);
                    return;
                }
            }

            Console.WriteLine($"{client.Client.RemoteEndPoint} faile to connect: Server is full!");
        }

        private static void UDPReceiveCallback(IAsyncResult result)
        {
            try
            {
                IPEndPoint clientEndPoint = new IPEndPoint(IPAddress.Any, 0);
                byte[] data = udpListener.EndReceive(result, ref clientEndPoint);
                udpListener.BeginReceive(UDPReceiveCallback, null);

                if (data.Length < 4)
                {
                    return;
                }

                using (Packet packet = new Packet(data))
                {
                    int clientId = packet.ReadInt();

                    if (clientId == 0)
                    {
                        return;
                    }

                    if (clients[clientId].udp.endPoint == null)
                    {
                        clients[clientId].udp.Connect(clientEndPoint);
                        return;
                    }

                    if (clients[clientId].udp.endPoint.ToString() == clientEndPoint.ToString())
                    {
                        clients[clientId].udp.HandleData(packet);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error receiving UDP data: {ex}.");
            }
        }

        public static void SendUDPData(IPEndPoint clientEndPoint, Packet packet)
        {
            try
            {
                if (clientEndPoint != null)
                {
                    udpListener.BeginSend(packet.ToArray(), packet.Length(), clientEndPoint, null, null);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error sending data to {clientEndPoint} via UDP: {ex}.");
            }
        }

        private static void InitializeServerData()
        {
            for (int i = 1; i <= MaxPlayers; i++)
            {
                clients.Add(i, new Client(i));
            }

            packetHandlers = new Dictionary<int, PacketHandler>()
            {
                { (int)ClientPackets.welcomeReceived, ServerHandle.WelcomeReceived },
                { (int)ClientPackets.roomsStateRequest, ServerHandle.RoomsStateRequestReceived },
                { (int)ClientPackets.makeMoveRequest, ServerHandle.MoveReceived },
                { (int)ClientPackets.createNewRoomRequest, ServerHandle.NewRoomReceived },
                { (int)ClientPackets.playerJoinRoomRequest, ServerHandle.PlayerJoinedRoomReceived },
                { (int)ClientPackets.playerLeaveRoomRequest, ServerHandle.PlayerLeftRoomReceived },
                { (int)ClientPackets.playerReadyStateChangeRequest, ServerHandle.PlayerReadyStateChangeReceived }
            };
            Console.WriteLine("Initialized packets.");
        }
    }
}
