﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TicTacToeClient
{
    public class Room : MonoBehaviour
    {
        [SerializeField] private Text gameIdText;
        [SerializeField] private Text playersText;
        [SerializeField] private Button connectButton;

        private int gameId;

        private void Awake()
        {
            connectButton.onClick.AddListener(ConnectToGame);
        }

        public void SetGameInfo(int gameId, int players)
        {
            this.gameId = gameId;

            gameIdText.text = $"Game ID: {gameId}";
            playersText.text = $"Players: {players}";
        }

        private void ConnectToGame()
        {
            UIManager.instance.ConnectToRoomRequest(gameId);
        }
    }
}
