﻿using System.Collections.Generic;
using System.Linq;

namespace TicTacToeServer
{
    class ServerSend
    {
        private static void SendTCPData(int toClient, Packet packet)
        {
            packet.WriteLength();
            Server.clients[toClient].tcp.SendData(packet);
        }

        private static void SendTCPDataToAll(IEnumerable<int> toClients, Packet packet)
        {
            packet.WriteLength();
            foreach (int toClient in toClients)
            {
                Server.clients[toClient].tcp.SendData(packet);
            }
        }

        private static void SendTCPDataToAll(Packet packet)
        {
            packet.WriteLength();
            for (int i = 1; i <= Server.MaxPlayers; i++)
            {
                Server.clients[i].tcp.SendData(packet);
            }
        }

        private static void SendTCPDataToAll(int exceptClient, Packet packet)
        {
            packet.WriteLength();
            for (int i = 1; i <= Server.MaxPlayers; i++)
            {
                if (i != exceptClient)
                {
                    Server.clients[i].tcp.SendData(packet);
                }
            }
        }

        private static void SendUDPData(int toClient, Packet packet)
        {
            packet.WriteLength();
            Server.clients[toClient].udp.SendData(packet);
        }

        private static void SendUDPDataToAll(Packet packet)
        {
            packet.WriteLength();
            for (int i = 1; i <= Server.MaxPlayers; i++)
            {
                Server.clients[i].udp.SendData(packet);
            }
        }

        private static void SendUDPDataToAll(int exceptClient, Packet packet)
        {
            packet.WriteLength();
            for (int i = 1; i <= Server.MaxPlayers; i++)
            {
                if (i != exceptClient)
                {
                    Server.clients[i].udp.SendData(packet);
                }
            }
        }

        #region Packets
        public static void Welcome(int toClient, string msg)
        {
            using (Packet packet = new Packet((int)ServerPackets.welcome))
            {
                packet.Write(msg);
                packet.Write(toClient);

                SendTCPData(toClient, packet);
            }
        }

        public static void RoomsState(int toClient)
        {
            using (Packet packet = new Packet((int)ServerPackets.roomsState))
            {
                //Send current rooms' state
                packet.Write(Server.rooms.Count);
                for (int i = 0; i < Server.rooms.Count; i++)
                {
                    RoomManager room = Server.rooms.Values.ElementAt(i);
                    packet.Write(room.roomId);
                    packet.Write(room.playersReady.Count);
                    for (int j = 0; j < room.playersReady.Count; j++)
                    {
                        var playerReady = room.playersReady.ElementAt(j);
                        packet.Write(playerReady.Key);
                        packet.Write(playerReady.Value);
                    }
                }

                SendTCPData(toClient, packet);
            }
        }

        public static void RoomCreated(int toClient, int roomId)
        {
            using (Packet packet = new Packet((int)ServerPackets.roomCreated))
            {
                packet.Write(toClient);
                packet.Write(roomId);

                SendTCPData(toClient, packet);
            }
        }

        public static void MakeMove(int roomId, bool isXTurn, int x, int y)
        {
            using (Packet packet = new Packet((int)ServerPackets.makeMove))
            {
                packet.Write(isXTurn);
                packet.Write(x);
                packet.Write(y);

                SendTCPDataToAll(Server.rooms[roomId].playersReady.Keys, packet);
            }
        }

        public static void PlayerJoinedRoom(int client, int roomId)
        {
            using (Packet packet = new Packet((int)ServerPackets.playerJoinedRoom))
            {
                packet.Write(client);
                packet.Write(roomId);

                SendTCPDataToAll(Server.rooms[roomId].playersReady.Keys, packet);
            }
        }

        public static void PlayerLeftRoom(int client, int roomId)
        {
            using (Packet packet = new Packet((int)ServerPackets.playerLeftRoom))
            {
                packet.Write(client);

                SendTCPDataToAll(Server.rooms[roomId].playersReady.Keys, packet);
            }
        }

        public static void PlayerReadyStateChange(int client)
        {
            using (Packet packet = new Packet((int)ServerPackets.playerReadyStateChange))
            {
                packet.Write(client);
                packet.Write(Server.clients[client].room.playersReady[client]);

                SendTCPDataToAll(Server.clients[client].room.playersReady.Keys, packet);
            }
        }

        public static void GameStarted(RoomManager room)
        {
            using (Packet packet = new Packet((int)ServerPackets.gameStarted))
            {
                SendTCPDataToAll(room.playersReady.Keys, packet);
            }
        }

        public static void GameEnded(RoomManager room)
        {
            using (Packet packet = new Packet((int)ServerPackets.gameEnded))
            {
                SendTCPDataToAll(room.playersReady.Keys, packet);
            }
        }
        #endregion

    }
}
