﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TicTacToeClient
{
    public class RoomManager
    {
        public int roomId;

        public Dictionary<int, bool> playersReady;

        public RoomManager(int id)
        {
            roomId = id;
            playersReady = new Dictionary<int, bool>();
        }

        public void PlayerJoin(int player)
        {
            playersReady.Add(player, false);
        }

        public void PlayerDisconnect(int player)
        {
            playersReady.Remove(player);
        }

        public void PlayerChangeReadyState(int player, bool ready)
        {
            playersReady[player] = ready;
        }

    }
}