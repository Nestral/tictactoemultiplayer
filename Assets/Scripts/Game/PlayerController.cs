﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TicTacToeClient
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private Camera PlayerCamera;

        private void Start()
        {
            if (PlayerCamera == null)
                PlayerCamera = Camera.main;
        }

        private void Update()
        {
            if (SystemInfo.deviceType == DeviceType.Desktop)
                HandleDesktopInput();
            else if (SystemInfo.deviceType == DeviceType.Handheld)
                HandleHandheldInput();
        }

        private Vector3 mousePosMemory;
        private void HandleDesktopInput()
        {
            if (Input.GetMouseButtonDown(0))
            {
                mousePosMemory = Input.mousePosition;
            }    

            if (Input.GetMouseButtonUp(0) && mousePosMemory == Input.mousePosition
                && GameManager.instance.isStarted && AimingAtCell(out Cell cell))
            {
                ClientSend.MakeMoveRequest(cell.position);
            }
        }

        private Vector2 touchPosMemory;
        private void HandleHandheldInput()
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                touchPosMemory = Input.GetTouch(0).position;
            }

            if (Input.GetTouch(0).phase == TouchPhase.Ended && touchPosMemory == Input.GetTouch(0).position
                && GameManager.instance.isStarted && AimingAtCell(out Cell cell))
            {
                ClientSend.MakeMoveRequest(cell.position);
            }
        }

        private bool AimingAtCell(out Cell cell)
        {
            cell = null;
            if (!RaycastFromCamera(out RaycastHit hit))
                return false;

            try
            {
                return hit.transform.parent.parent.TryGetComponent(out cell);
            }
            catch (System.Exception)
            {
                return false;
            }
        }

        private bool RaycastFromCamera(out RaycastHit raycastHit)
        {
            Ray ray = PlayerCamera.ScreenPointToRay(Input.mousePosition);
            return Physics.Raycast(ray, out raycastHit);
        }
    }
}