﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TicTacToeServer
{
    class RoomManager
    {
        public readonly int roomId;

        private readonly int maxPlayers;
        private readonly int minPlayers;
        private readonly GameManager game;

        public readonly Dictionary<int, bool> playersReady;

        public RoomManager(int _maxPlayers, int _minPlayers, int id)
        {
            maxPlayers = _maxPlayers;
            minPlayers = _minPlayers;
            roomId = id;

            game = new GameManager(this);
            playersReady = new Dictionary<int, bool>();
        }

        public bool PlayerJoin(int player)
        {
            if (playersReady.Count == maxPlayers)
            {
                return false;
            }

            if (playersReady.Count == 0)
                game.xPlayer = player;
            else
                game.oPlayer = player;

            Server.clients[player].room = this;
            playersReady.Add(player, false);
            return true;
        }

        public void PlayerDisconnect(int player)
        {
            if (game.xPlayer == player)
            {
                game.xPlayer = game.oPlayer;
                game.oPlayer = 0;
            }
            else
            {
                game.oPlayer = 0;
            }

            Server.clients[player].room = null;
            playersReady.Remove(player);

            if (playersReady.Count == 0)
            {
                Server.RemoveRoom(this);
            }
        }

        public void PlayerChangeReadyState(int player)
        {
            playersReady[player] = !playersReady[player];

            bool readyToStart = true;
            if (playersReady.Count < minPlayers)
                readyToStart = false;
            else
            {
                foreach (bool isReady in playersReady.Values)
                {
                    if (!isReady)
                    {
                        readyToStart = false;
                        break;
                    }
                }
            }

            if (readyToStart)
            {
                StartGame();
            }
        }

        public void StartGame()
        {
            game.StartGame();

            Console.WriteLine($"The game in room {roomId} has started.");
            ServerSend.GameStarted(this);
        }

        public void GameEnded()
        {
            //Reset players' ready state
            int[] clients = playersReady.Keys.ToArray();
            foreach (int client in clients)
            {
                playersReady[client] = false;
            }

            Console.WriteLine($"The game in room {roomId} has ended.");
            ServerSend.GameEnded(this);
        }

        public void MoveReceived(int fromClient, int x, int y)
        {
            // Console.WriteLine($"Received a move request from player {fromClient}.");

            if (!game.isStarted || game.isFinished)
            {
                //Console.WriteLine($"Game is either not started or already finished. Unable to make a move.");
                return;
            }

            int playerTurn = game.isXTurn ? game.xPlayer : game.oPlayer;

            if (playerTurn != fromClient)
            {
                // Console.WriteLine($"Player is trying to make a move while it's {playerTurn}'s move right now.");
                return;
            }

            game.MakeMove(x, y);
        }

    }
}
