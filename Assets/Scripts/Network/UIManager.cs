﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using UnityEngine.UI;

namespace TicTacToeClient
{
    public class UIManager : MonoBehaviour
    {
        public static UIManager instance;

        [SerializeField, Range(0.1f, 60f)]
        private float roomsStateRequestPeriod = 5f;

        public InputField usernameField;
        public InputField ipField;
        public InputField roomIdField;
        public Text gameIdText;

        public GameObject roomPrefab;
        public GameObject roomPlayerPrefab;

        public GameObject serverConnectMenu;
        public GameObject roomConnectMenu;
        public GameObject roomsHolder;
        public GameObject roomMenu;
        public GameObject roomPlayers;
        public GameObject gameMenu;
        public GameObject gameResultsMenu;

        private GameObject activeMenu;

        internal Dictionary<int, RoomManager> playerRooms = new Dictionary<int, RoomManager>();
        internal Dictionary<int, RoomManager> rooms = new Dictionary<int, RoomManager>();

        private Dictionary<int, RoomPlayer> players = new Dictionary<int, RoomPlayer>();

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else if (instance != this)
            {
                Debug.LogWarning("Instance already exists, destroying object!");
                Destroy(this);
            }

            activeMenu = serverConnectMenu;
            activeMenu.SetActive(true);
        }

        public void ConnectToServer()
        {
            IPAddress serverIP;
            if (ipField.text == "")
            {
                Debug.Log("Connecting to local host...");
                serverIP = IPAddress.Parse("127.0.0.1");
            }
            else
            {
                IPHostEntry serverHostEntry;
                try
                {
                    serverHostEntry = Dns.GetHostEntry(ipField.text);
                }
                catch
                {
                    Debug.Log("Unable to find host with specified name or address.");
                    return;
                }

                Debug.Log($"Found host named {serverHostEntry.HostName}. Connecting...");
                serverIP = serverHostEntry.AddressList[0];
            }

            Client.instance.ConnectToServer(serverIP);
        }

        public void CreateNewRoomRequest()
        {
            Client.instance.CreateNewRoomRequest();
        }

        public void ConnectToRoomRequest(int roomId)
        {
            Client.instance.ConnectToRoomRequest(roomId);
        }

        public void ConnectToRoomRequest()
        {
            if (int.TryParse(roomIdField.text, out int roomId))
            {
                Client.instance.ConnectToRoomRequest(roomId);
            }
        }

        public void ChangeReadyStateRequest()
        {
            Client.instance.ChangeReadyStateRequest();
        }

        public void LeaveServerRequest()
        {
            //Client.instance.LeaveServerRequest();
        }
        public void LeaveRoomRequest()
        {
            Client.instance.LeaveRoomRequest();
        }

        public void RoomsStateRequest()
        {
            ClientSend.RoomsStateRequest();
        }

        public void ClearInformation()
        {
            int roomsCount = roomsHolder.transform.childCount;
            for (int i = 0; i < roomsCount; i++)
            {
                Destroy(roomsHolder.transform.GetChild(i).gameObject);
            }

            playerRooms.Clear();
            rooms.Clear();
        }

        public void OnRoomsStateInfoReceived()
        {
            //Only show if you are in room choose menu
            if (activeMenu != roomConnectMenu)
                return;

            foreach (RoomManager room in rooms.Values)
            {
                Room roomUI = Instantiate(roomPrefab, roomsHolder.transform).GetComponent<Room>();
                roomUI.SetGameInfo(room.roomId, room.playersReady.Count);
            }
        }

        public void OnConnectedToServer()
        {
            StartCoroutine(RequestRoomsStateCoroutine());

            SetActiveMenu(roomConnectMenu);
        }

        public void OnRoomCreated(int clientId, int roomId)
        {
            //Create new room
            RoomManager room = new RoomManager(roomId);

            rooms.Add(roomId, room);

            //Connect to room
            OnPlayerConnectedToRoom(clientId, roomId);
        }

        public void OnPlayerConnectedToRoom(int clientId, int roomId)
        {
            if (playerRooms.TryGetValue(Client.instance.myId, out RoomManager myRoom) && myRoom.roomId != roomId)
                return;

            RoomManager room = rooms[roomId];

            //If local player, switch scene
            if (clientId == Client.instance.myId)
            {
                StopCoroutine(RequestRoomsStateCoroutine());

                SetActiveMenu(roomMenu);
                gameIdText.text = "You game ID is: " + room.roomId.ToString();

                //UI
                foreach (var client in room.playersReady)
                {
                    RoomPlayer player = Instantiate(roomPlayerPrefab, roomPlayers.transform).GetComponent<RoomPlayer>();
                    players.Add(client.Key, player);
                    player.SetPlayerReady(client.Value);
                }
            }

            //Connect
            room.PlayerJoin(clientId);
            playerRooms.Add(clientId, room);

            //UI
            RoomPlayer roomPlayer = Instantiate(roomPlayerPrefab, roomPlayers.transform).GetComponent<RoomPlayer>();
            players.Add(clientId, roomPlayer);
        }

        public void OnDisconnectedFromServer()
        {
            StopCoroutine(RequestRoomsStateCoroutine());

            SetActiveMenu(serverConnectMenu);
        }

        public void OnPlayerDisconnectedFromRoom(int clientId)
        {
            RoomManager room = playerRooms[clientId];

            room.PlayerDisconnect(clientId);
            Destroy(players[clientId].gameObject);
            players.Remove(clientId);
            playerRooms.Remove(clientId);

            //Local player
            if (clientId == Client.instance.myId)
            {
                StartCoroutine(RequestRoomsStateCoroutine());

                SetActiveMenu(roomConnectMenu);

                foreach (RoomPlayer player in players.Values)
                {
                    Destroy(player.gameObject);
                }
            }
        }

        public void OnPlayerReadyStateChanged(int clientId, bool ready)
        {
            RoomManager room = playerRooms[clientId];

            room.PlayerChangeReadyState(clientId, ready);
            players[clientId].SetPlayerReady(ready);
        }

        public void OnGameStarted()
        {
            GameManager.instance.StartGame();
            SetActiveMenu(gameMenu);
        }

        public void OnGameEnded()
        {
            GameManager.instance.EndGame();
            SetActiveMenu(roomMenu);

            foreach (RoomPlayer player in players.Values)
            {
                player.SetPlayerReady(false);
            }
        }

        private IEnumerator RequestRoomsStateCoroutine()
        {
            while (true)
            {
                RoomsStateRequest();

                yield return new WaitForSecondsRealtime(roomsStateRequestPeriod);
            }
        }

        private void SetActiveMenu(GameObject menu)
        {
            activeMenu.SetActive(false);
            activeMenu = menu;
            activeMenu.SetActive(true);
        }

    }
}