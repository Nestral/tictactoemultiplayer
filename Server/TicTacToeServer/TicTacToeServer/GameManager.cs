﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TicTacToeServer
{
    class GameManager
    {
        public enum Mark { X, O, E };

        public Mark[,] map;
        public bool isXTurn = true;
        public bool isStarted = false;
        public bool isFinished = false;

        public int xPlayer;
        public int oPlayer;

        private readonly RoomManager room;

        public GameManager(RoomManager _room)
        {
            room = _room;

            ResetMap();
        }

        public void ResetMap()
        {
            map = new Mark[,]
            {
                {Mark.E, Mark.E, Mark.E },
                {Mark.E, Mark.E, Mark.E },
                {Mark.E, Mark.E, Mark.E }
            };
            isXTurn = true;
            isStarted = false;
            isFinished = false;
        }

        public void StartGame()
        {
            if (isStarted)
            {
                ResetMap();
            }

            isStarted = true;
        }

        public void MakeMove(int x, int y)
        {
            if (!isStarted || isFinished)
                return;

            if (x < 0 || y < 0 || x >= map.GetLength(0) || y >= map.GetLength(1) || map[x, y] != Mark.E)
                return;

            map[x, y] = isXTurn ? Mark.X : Mark.O;

            ServerSend.MakeMove(room.roomId, isXTurn, x, y);
            isXTurn = !isXTurn;

            if (IsGameFinished())
            {
                isFinished = true;
                room.GameEnded();
                return;
            }
        }

        private bool IsGameFinished()
        {
            //Check empty spaces
            bool hasEmptySpaces = false;
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (map[i, j] == Mark.E)
                    {
                        hasEmptySpaces = true;
                        break;
                    }
                }
                if (hasEmptySpaces)
                    break;
            }

            if (!hasEmptySpaces)
                return true;

            //Check horizontals
            for (int y = 0; y < 3; y++)
            {
                if (map[0, y] != Mark.E && map[0, y] == map[1, y] && map[1, y] == map[2, y])
                    return true;
            }

            //Check verticals
            for (int x = 0; x < 3; x++)
            {
                if (map[x, 0] != Mark.E && map[x, 0] == map[x, 1] && map[x, 1] == map[x, 2])
                    return true;
            }

            //Check diagonals
            if (map[0, 0] != Mark.E && map[0, 0] == map[1, 1] && map[1, 1] == map[2, 2])
                return true;
            if (map[0, 2] != Mark.E && map[0, 2] == map[1, 1] && map[1, 1] == map[2, 0])
                return true;

            return false;
        }

    }
}
