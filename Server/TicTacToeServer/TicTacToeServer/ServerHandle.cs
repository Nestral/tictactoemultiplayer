﻿using System;

namespace TicTacToeServer
{
    class ServerHandle
    {
        #region Packets
        public static void WelcomeReceived(int fromClient, Packet packet)
        {
            int clientIdCheck = packet.ReadInt();
            string username = packet.ReadString();

            Console.WriteLine($"{Server.clients[fromClient].tcp.socket.Client.RemoteEndPoint} connected successfully and is now player {fromClient}.");
            if (fromClient != clientIdCheck)
            {
                Console.WriteLine($"Player \"{username}\" (ID: {fromClient}) has assumed the wrong client ID ({clientIdCheck})!");
                return;
            }
        }

        public static void RoomsStateRequestReceived(int fromClient, Packet packet)
        {
            ServerSend.RoomsState(fromClient);
        }

        public static void MoveReceived(int fromClient, Packet packet)
        {
            int x = packet.ReadInt();
            int y = packet.ReadInt();

            RoomManager playerRoom = Server.clients[fromClient].room;

            if (playerRoom == null)
            {
                return;
            }

            playerRoom.MoveReceived(fromClient, x, y);
        }

        public static void NewRoomReceived(int fromClient, Packet packet)
        {
            RoomManager newRoom = Server.CreateNewRoom(fromClient);

            Console.WriteLine($"Player {fromClient} has created a new room {newRoom.roomId}.");
            ServerSend.RoomCreated(fromClient, newRoom.roomId);
        }

        public static void PlayerJoinedRoomReceived(int fromClient, Packet packet)
        {
            int clientId = packet.ReadInt();
            int roomId = packet.ReadInt();

            if (!Server.rooms.ContainsKey(roomId))
            {
                // Console.WriteLine($"Player {fromClient} trying to join room {roomId} which doesn't exist.");
                return;
            }

            if (!Server.rooms[roomId].PlayerJoin(fromClient))
            {
                Console.WriteLine($"Player {fromClient} couldn't join room {roomId}, because it is already full.");
                return;
            }

            Console.WriteLine($"Player {fromClient} has joined the room {roomId}.");

            ServerSend.PlayerJoinedRoom(fromClient, roomId);
        }

        public static void PlayerLeftRoomReceived(int fromClient, Packet packet)
        {
            int clientId = packet.ReadInt();

            //Check if player is connected to a room
            RoomManager room = Server.clients[clientId].room;
            if (room == null)
            {
                return;
            }

            int roomId = room.roomId;

            //Disconnect from room
            Console.WriteLine($"Player {fromClient} has left the room {roomId}.");

            ServerSend.PlayerLeftRoom(fromClient, roomId);
            room.PlayerDisconnect(fromClient);
        }

        public static void PlayerReadyStateChangeReceived(int fromClient, Packet packet)
        {
            int clientId = packet.ReadInt();
            Server.clients[fromClient].room.PlayerChangeReadyState(fromClient);

            ServerSend.PlayerReadyStateChange(fromClient);
            Console.WriteLine($"Player {fromClient} has changed his ready state.");
        }
        #endregion

        public static void PlayerDisconnect(int client)
        {
            RoomManager room = Server.clients[client].room;
            if (room != null)
                room.PlayerDisconnect(client);
        }
    }
}
